# M06-ASO

## @edt ASIX M06 2021-2022

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació i els dokerfiles del mòdul a [ASIX-M06](https://github.com/edtasixm06)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)


---

### Servei SAMBA
 
 * [HowTo-ASIX-SAMBA.pdf](https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_5_samba/HowTo-ASIX-SAMBA-part2-userShares-2016-2017.pdf)
 * [Using Samba, 3rd, O'Reilly & Associates (2007)](https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_5_samba/Using%20Samba,%203Rd%20Edition%20(2007)%20-%20Oreilly.pdf)
