# M06-ASO

## @edt ASIX M06 2021-2022

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació i els dokerfiles del mòdul a [ASIX-M06](https://github.com/edtasixm06)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

[ASIX-M01](https://gitlab.com/edtasixm01/m01-operatius)  |  [ASIX-M05](https://gitlab.com/edtasixm05/m05-hardware) |  [ASIX-M06](https://gitlab.com/edtasixm06/m06-aso) |  [ASIX-M11](https://gitlab.com/edtasixm11/m11-sad)

---

### UF1 Administració avançada de sistemes operatius

 * Servei de directori LDAP
 * Autenticació PAM
 * Accés Remot: ssh, rsync, remina, vnc...
 * Servei d'impressió CUPS
 * Sistemes heterogenis: SAMBA
 * Processos


---

### UF2 Automatització de tasques i llenguatge de guions 

 * Named pipes
 * Signals
 * Sockets
 * fork/execv
 

---
